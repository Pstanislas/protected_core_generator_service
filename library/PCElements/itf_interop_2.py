# Log imports
import logging
import logging.config
import uuid

class Itf_interop_2:
    """ Class definition of a Itf_interop_2 """

    def __init__(self, cc, pf, number):
        """ Constructor """

        self.id = str(uuid.uuid4())
        self.name = 'ITF2-' + pf.name + "." + str(number)
        self.cc = cc
        self.pf = pf
        self.hostName = pf.name

        # Init the Itf_interop_2 instance object related logger.
        self.logger = logging.getLogger('itf2')
        self.logger.name = self.name

        self.link = None

        self.logger.info(f"{self.name} has been created.")