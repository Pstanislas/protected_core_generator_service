# Log imports
import logging
import logging.config
import uuid
import random
import json

from library.PCElements import protected_core_segment as PCS
from library.PCElements import colored_cloud as CC

class Protected_core:
    """ Class definition of a Protected Core. """
    
    def __init__(self, PCSList, CCList):
        """ Constructor """

        self.logger = logging.getLogger('protectedCore')

        self.id = str(uuid.uuid4())
        self.name = "pc0"
        self.PCSList = PCSList
        self.CCList = CCList

        self.logger.info(f"PC has been created")
        self.dataLogPrint()

    def dataLogPrint(self):
        """ Create a global PC data log. """

        # Create a PCS log String by tacking all PCS id related to the PC
        PCSString = "["

        for pcs in self.PCSList:
            PCSString = PCSString + "(" + str(pcs.name) + ")"
        
        PCSString = PCSString + "]"

        # Create a CC log String by tacking all CC id related to the PC
        CCString = "["

        for cc in self.CCList:
            CCString = CCString + "(" + str(cc.name) + ")"
        
        CCString = CCString + "]"

        self.logger.data(f"PC {self.name}, PCS list = {PCSString}")
        self.logger.data(f"PC {self.name}, CC  list = {CCString}")

    # ---------------------------------------------
    # Protected Core segment related functions ----
    #----------------------------------------------

    def addProtectedCoreSegment(self, pcs):
        """ Add a PCS (passed in parameter) to the PCS list. """
        self.PCSList.append(pcs)
        self.logger.info(f"{pcs.name} has been added to the pcs List")

    def createRandomProtectedCoreSegment(self, paramDictionary):
        """ Create a random PCS. Take in parameter the parmDictionary to define the number of eNodes. """
        
        # get a random number of EN for this PCS regarding nbENMin and nbENMax in param conf file
        randNbEn = random.choice(range(paramDictionary['PCSINFO'].get("nbENMin"),paramDictionary['PCSINFO'].get("nbENMax")+1))
        pcs = PCS.Protected_core_segment(len(self.PCSList))
        
        # Create all E-nodes of the PCS
        pcs.createMultipleENodes(randNbEn)
        
        # Create all endpoint_pair_links of the PCS
        pcs.createFullMeshEPPLinks()
        
        self.logger.info(f"Random {pcs.name} with {randNbEn} EN has been created.")
        
        self.addProtectedCoreSegment(pcs)

    def createMultipleRandomProtectedCoreSegments(self, paramDictionary):
        """ Create nbPcs random PCS. Take in parameter the parmDictionary to define the amount of eNodes and the nbPcs the total amount of PCS to create. """
        
        nbPcs = paramDictionary['PCSINFO'].get("nbPCS")

        for number in range(0,nbPcs):
            self.createRandomProtectedCoreSegment(paramDictionary)

    def createRandomPCN1LinkTopology(self, paramDictionary):
        """ Create a random PCN1 Topology"""

        nbMinRelation = paramDictionary['PCSINFO'].get("nbMinRelation")
        nbMaxRelation = paramDictionary['PCSINFO'].get("nbMaxRelation")

        self.logger.info(f"Start creating Random PCN1 Link Topology.")

        # For each PCS
        for pcs in self.PCSList:
            
            # define a random number of external pcn1 relations with others pcn
            randNbPCN1L = random.choice(range(nbMinRelation,nbMaxRelation+1))
            self.logger.data(f"{pcs.name} will have {randNbPCN1L} PCN1 links")

            pcsAvailableList = [element for element in self.PCSList if element != pcs ]
            printablePcsAvailableList = []

            for pcsDest in pcsAvailableList:
                printablePcsAvailableList.append(pcsDest.name)
            
            self.logger.data(f"pcsAvailableList = {printablePcsAvailableList}")

            eNodeDestinationList = []

            # create a random list of eNodes destination regarding the random number of destination
            for number in range(0,randNbPCN1L):
                
                # select a random pcs of the pcsAvailableList
                pcsDestination = random.choice(pcsAvailableList)
                # select a random eNode of this pcs
                eNodeDestination = random.choice(pcsDestination.listEN)
                eNodeDestinationList.append(eNodeDestination)

            printableENodeDestinationList = []

            for eNodeDest in eNodeDestinationList:
                printableENodeDestinationList.append(eNodeDest.name)

            self.logger.data(f"eNodeDestinationList = {printableENodeDestinationList}")

            # for each e-node of the destination list create the link between it and a random eNode of the pcs.
            for eNodeDest in eNodeDestinationList:
                # select a random source eNode
                eNodeSource = random.choice(pcs.listEN)
                
                # create interfaces
                itf1InterfaceSource = eNodeSource.createITF1Interface()
                itf1InterfaceDest = eNodeDest.createITF1Interface()
                
                # create the link
                eNodeSource.createPcn1Link(itf1InterfaceSource, itf1InterfaceDest)

                # for reverse link (possible to add more option)
                isBiDirectional = True
                
                if isBiDirectional:
                    # create the link in the reverse way
                    itf1InterfaceSource = eNodeDest.createITF1Interface()
                    itf1InterfaceDest = eNodeSource.createITF1Interface()
                    
                    eNodeDest.createPcn1Link(itf1InterfaceSource, itf1InterfaceDest)

                self.logger.info(f"A link between {eNodeSource.name} and {eNodeDest.name}  has been created")

    def setAllRandomPcsCapacity(self, paramDictionary, vnfCatalog):
        """ Function to add random capacity on all PCS (link and e-nodes) """

        # For each pcs, make random capacity (enode)
        self.logger.info(f"Start random all PCSs Capacity")
        for pcs in self.PCSList:
            
            # define a random number of intelligent E-nodes
            nbMinVnfNode = paramDictionary['CAPACITY'].get('nbMinVnfNode')
            nbMaxVnfNode = paramDictionary['CAPACITY'].get('nbMaxVnfNode')

            # check if the random number max is < compare to the amount of e-node
            if nbMaxVnfNode > len(pcs.listEN):
                nbMaxVnfNode = len(pcs.listEN)

            if nbMinVnfNode > len(pcs.listEN):
                nbMinVnfNode = len(pcs.listEN)

            randNbENode = random.choice(range(nbMinVnfNode,nbMaxVnfNode+1))
            self.logger.info(f"{pcs.name} will have {randNbENode} intelligent E-nodes")

            selectedENodeList = []
            availableENodeList = [element for element in pcs.listEN]

            # select randomly randNbENode e-nodes of the PCS which ones will be intelligent
            for number in range(0,randNbENode):
                    
                    # select a random pcs of the pcsAvailableList
                    # select a random eNode of this pcs who is not already taken
                    randomENode = random.choice(availableENodeList)
                    selectedENodeList.append(randomENode)
                    availableENodeList.remove(randomENode)

            # For each intelligent e-node of the subset
            for eNode in selectedENodeList:
                minVnfCapacity = paramDictionary['CAPACITY'].get('minVnfCapacity')
                maxVnfCapacity = paramDictionary['CAPACITY'].get('maxVnfCapacity')
                
                # Define a random capacity -----------------
                randomCapacity = random.choice(range(minVnfCapacity,maxVnfCapacity+1))
                eNode.capacity = randomCapacity
                eNode.isIntelligent = True

                # Define a random List of available VNF -----------------                
                # Define how many different vnf the e-node will have
                minVnfAvailableFromCatalog = paramDictionary['CAPACITY'].get('minVnfAvailableFromCatalog')
                maxVnfAvailableFromCatalog = paramDictionary['CAPACITY'].get('maxVnfAvailableFromCatalog')

                randomAvailableVnfNumber = random.choice(range(minVnfAvailableFromCatalog,maxVnfAvailableFromCatalog+1))

                # Select randoms vnf from the vnf catalog and add them to the intelligent e-node.
                selectedVnfList = random.sample(vnfCatalog,randomAvailableVnfNumber)

                eNode.vnfAvailabeCatalog = selectedVnfList

                
            # Define capacity link information
            # For each E-node
            for eNode in pcs.listEN:
                # For each PCN1 Link
                for link in eNode.listPCN1L:
                    
                    # Define random capacity
                    minLinkCapacity = paramDictionary['CAPACITY'].get('minPcs1LinkCapacity')
                    maxLinkCapacity = paramDictionary['CAPACITY'].get('maxPcs1LinkCapacity')
                    randLinkCapacity = random.choice(range(minLinkCapacity,maxLinkCapacity+1))

                    # Add capacity
                    link.capacity = randLinkCapacity 

                # For each PCN2 Link
                for link in eNode.listPCN2L:
                    
                    # Define random capacity
                    minLinkCapacity = paramDictionary['CAPACITY'].get('minPcs2LinkCapacity')
                    maxLinkCapacity = paramDictionary['CAPACITY'].get('maxPcs2LinkCapacity')
                    randLinkCapacity = random.choice(range(minLinkCapacity,maxLinkCapacity+1))

                    # Add capacity
                    link.capacity = randLinkCapacity

                # For each EPP Link
                for link in eNode.listEPPL:
                    
                    # Define random capacity
                    minLinkCapacity = paramDictionary['CAPACITY'].get('minEppLinkCapacity')
                    maxLinkCapacity = paramDictionary['CAPACITY'].get('maxEppLinkCapacity')
                    randLinkCapacity = random.choice(range(minLinkCapacity,maxLinkCapacity+1))

                    # Add capacity
                    link.capacity = randLinkCapacity

    #----------------------------------------------
    # Colored Cloud related functions -------------
    #----------------------------------------------

    def addColoredCloud(self, cc):
        """ Add a cc (passed in parameter) to the cc list. """
        self.CCList.append(cc)
        self.logger.data(f"{cc.name} has been added to the cc List")

    def createRandomColoredCloud(self, paramDictionary):
        """ Create a random cc. Take in parameter the parmDictionary to define the number of eNodes. """
        
        # get a random number of PF for this CC regarding nbPFmin and nbPFmax
        randNbPf = random.choice(range(paramDictionary['CCINFO'].get("nbPFMin"),paramDictionary['CCINFO'].get("nbPFMax")+1))
        
        # get a random color for the colored cloud regarding nbDomaine parameter.
        randColor = random.choice(range(0, paramDictionary['CCINFO'].get("nbColor")))
        nbColor = paramDictionary['CCINFO'].get("nbColor")
        cc = CC.Colored_cloud(len(self.CCList), randNbPf, randColor)

        # Create all E-nodes of the PCS
        cc.createMultiplePF(randNbPf)
        
        # Create all endpoint_pair_links of the PCS
        cc.createFullMeshEPPLinks()

        self.logger.info(f"Random {cc.name} with {randNbPf} PF with color {randColor} has been created.")

        self.addColoredCloud(cc)

    def createMultipleRandomColoredCloud(self, paramDictionary):
        """ Create nbCc random CC. Take in parameter the parmDictionary to define the amount of eNodes and the nbCc the total amount of CC to create. """
        
        for number in range(0,paramDictionary['CCINFO'].get("nbCC")):
            self.createRandomColoredCloud(paramDictionary)

    def createRandomPCN2LinkTopology(self, paramDictionary):
        """ Create a random PCN2 Topology"""

        nbMinRelationCC = paramDictionary['CCINFO'].get("nbMinRelationCC")
        nbMaxRelationCC = paramDictionary['CCINFO'].get("nbMaxRelationCC")

        self.logger.info(f"Start creating Random PCN1 Link Topology.")

        # For each PCS
        for cc in self.CCList:
            
            # define a random number of external pcn2 relations with others pcn
            randNbPCN2L = random.choice(range(nbMinRelationCC,nbMaxRelationCC+1))
            self.logger.data(f"{cc.name} will have {randNbPCN2L} PCN2 links")

            pcsAvailableList = [element for element in self.PCSList]
            printablePcsAvailableList = []

            for pcsDest in pcsAvailableList:
                printablePcsAvailableList.append(pcsDest.name)
            
            self.logger.data(f"pcsAvailableList = {printablePcsAvailableList}")

            eNodeDestinationList = []

            # create a random list of eNodes destination regarding the random number of destination
            for number in range(0,randNbPCN2L):
                
                # select a random pcs of the pcsAvailableList
                pcsDestination = random.choice(pcsAvailableList)
                # select a random eNode of this pcs
                eNodeDestination = random.choice(pcsDestination.listEN)
                eNodeDestinationList.append(eNodeDestination)

            printableENodeDestinationList = []

            for eNodeDest in eNodeDestinationList:
                printableENodeDestinationList.append(eNodeDest.name)

            self.logger.data(f"eNodeDestinationList = {printableENodeDestinationList}")

            # for each e-nodeof the destination list create the link between it and a random p-functionality-node of the cc.
            for eNodeDest in eNodeDestinationList:
                # select a random source p-functionality-node
                pfNodeSource = random.choice(cc.listPF)
                
                # create interfaces
                itf2InterfaceSource = pfNodeSource.createITF2Interface()
                itf1InterfaceDest = eNodeDest.createITF1Interface()
                
                # create the link
                pfNodeSource.createPcn2Link(itf2InterfaceSource, itf1InterfaceDest)

                # for reverse link (possible to add more option)
                isBiDirectional = True
                
                if isBiDirectional:
                    # create the link in the reverse way
                    itf1InterfaceSource = eNodeDest.createITF1Interface()
                    itf2InterfaceDest = pfNodeSource.createITF2Interface()
                    
                    eNodeDest.createPcn2Link(itf1InterfaceSource, itf2InterfaceDest)

                self.logger.info(f"A link between {pfNodeSource.name} and {eNodeDest.name}  has been created")