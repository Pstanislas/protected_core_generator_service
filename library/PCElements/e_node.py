# Log imports
import logging
import logging.config
import uuid


from library.PCElements import endpoint_pair_link as EPPLink

from library.PCElements import pcn1_link as pcn1Link
from library.PCElements import itf_interop_1 as Ift1

from library.PCElements import pcn2_link as pcn2Link
from library.PCElements import itf_interop_2 as Ift2

class E_node:
    """ Class definition of a E_node """

    def __init__(self, pcs, number):
        """ Constructor """

        self.id = str(uuid.uuid4())
        self.name = 'EN-' + str(pcs.name) + "." + str(number)
        self.pcs = pcs
        
        self.capacity = 0
        self.isIntelligent = False
        self.vnfAvailabeCatalog = []

        # Init the E-node instance object related logger.
        self.logger = logging.getLogger('eNode')
        self.logger.name = self.name

        self.listItf1 = []
        self.nbItf1 = 0
        
        self.listEPPL = []
        self.nbEPPLink = 0

        self.listPCN1L = []
        self.nbPCN1Link = 0

        self.listPCN2L = []
        self.nbPCN2Link = 0

        self.logger.info(f"{self.name} has been created.")

    def createEPPLink(self,itf1Source, itf1Dest):
        """ Create and add an endpoint_pair_links between two interfaces """
        link = EPPLink.Endpoint_pair_link(self.pcs, self, itf1Source, itf1Dest, self.nbEPPLink)
        self.listEPPL.append(link)
        self.nbEPPLink+=1

    def createPcn1Link(self, itf1Source, itf1Dest):
        """ Create and add an pcn1_link between two interfaces """
        link = pcn1Link.Pcn1_link(self.pcs, self, itf1Source, itf1Dest, self.nbPCN1Link)
        self.listPCN1L.append(link)
        self.nbPCN1Link+=1

    def createPcn2Link(self, itf1Source, itf2Dest):
        """ Create and add an pcn2_link between two interfaces """
        link = pcn2Link.Pcn2_link(self.pcs, self, itf1Source, itf2Dest, self.nbPCN2Link)
        self.listPCN2L.append(link)
        self.nbPCN2Link+=1

    def createITF1Interface(self):
        """ Create and add an ITF1 interface and return it """
        newItf1 = Ift1.Itf_interop_1(self.pcs,self,self.nbItf1)
        self.listItf1.append(newItf1)
        self.nbItf1+=1
        
        return newItf1

    # VNF related functions -----------------------------
    
    def setCapacity(self, capacity):
        """ Set e-Node Capacity """
        self.capacity = capacity

    def setVnfAvailabeList(self, list):
        """ Set the vnf available list of the e-node """
        self.vnfAvailabeList = list