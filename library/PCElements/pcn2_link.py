from library.PCElements.link import Link

class Pcn2_link(Link):
    """ Class definition of a Pcn2_link with a Link heritage """

    # here it's call pcs and en ... but in reality ic can also be a cc and a pf
    def __init__(self, pcs, en, itfSource, itfDest, number):
        """ Constructor """

        # Heritage
        super().__init__(pcs, en)
        
        self.name = 'Pcn2L-' + str(en.name)+ "." + str(itfDest.hostName)+ ".l" + str(number)
        self.itfSource = itfSource
        self.itfDest = itfDest
        # Update the logger name of the link (pcs,source,destination,id)
        self.logger.name = self.name
        
        self.logger.info(f"Pcn2L {pcs.name} - (s){en.name} --> {itfDest.hostName}(d) - linkNumber:{number} has been created.")