# Log imports
import logging
import logging.config
import uuid

class Itf_interop_1:
    """ Class definition of a Itf_interop_1 """

    def __init__(self, pcs, en, number):
        """ Constructor """

        self.id = str(uuid.uuid4())
        self.name = 'ITF1-' + en.name + "." + str(number)
        self.pcs = pcs
        self.en = en
        self.hostName = en.name

        # Init the Itf_interop_1 instance object related logger.
        self.logger = logging.getLogger('itf1')
        self.logger.name = self.name

        self.link = None

        self.logger.info(f"{self.name} has been created.")