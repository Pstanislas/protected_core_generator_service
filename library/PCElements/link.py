# Log imports
import logging
import logging.config
import uuid

class Link:
    """ Class definition of a Link """

    def __init__(self, pcs, en):
        """ Constructor """

        # Init the E-node instance object related logger.
        self.logger = logging.getLogger('link')
        #self.ENLogger.name = 'EN-' + str(pcsId) + "." + str(id)
        
        self.id = str(uuid.uuid4())
        self.pcs = pcs
        self.en = en

        self.capacity = 0
        self.bandwidth = 0
        self.availability = 0
        self.restoration = 0
        self.maxDelay = 0
        self.information = "Infos"