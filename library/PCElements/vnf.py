# Log imports
import logging
import logging.config
import uuid

class Vnf:
    """ Class definition of a Vnf """

    def __init__(self,name,cost):
        """ Constructor """

        # Init the Vnf instance object related logger.
        self.logger = logging.getLogger('vnf')
        self.logger.name = 'vnf-' + name
        
        self.name = name
        self.id = str(uuid.uuid4())

        self.cost = cost
        self.information = "No Info"

        self.logger.info(f"vnf {self.name} init ok.")