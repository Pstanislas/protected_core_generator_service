# Log imports
import logging
import logging.config
import uuid

from library.PCElements import e_node as EN

class Protected_core_segment:
    
    def __init__(self, number):
        """ Constructor """

        # Init the PCS instance object related logger.
        self.logger = logging.getLogger('protectedCoreSegment')
        self.logger.name = 'PCS-' + str(number)
        
        self.id = str(uuid.uuid4())
        self.name = 'pcs'+str(number)
        self.listEN = []

        self.logger.info(f"PCS {number} has been created.")

    # ---------------------------------------------
    # E-nodes related functions -------------------
    #----------------------------------------------

    def addENode(self, en):
        """ Add an E-node to the E-node list of the PCS"""
        self.listEN.append(en)
        self.logger.info(f"{en.name} has been added to the e-node list.")

    def createENode(self):
        """ Create and add one E-node """

        number = len(self.listEN)
        en = EN.E_node(pcs=self,number=number)
        self.addENode(en)

    def createMultipleENodes(self, nbEn):
        """ Create nbEN E-nodes and add it to the e-node PCS list """

        for number in range(0,nbEn):
            self.createENode()

        self.logger.info(f"{nbEn} E-nodes has been added.")
    
    # ---------------------------------------------
    # Links related functions ----------------------
    #----------------------------------------------

    def createFullMeshEPPLinks(self):
        """ Create or recreate a full mesh topology endpoint_pair_links on the PCS """

        for en in self.listEN:
        # Remove itf1 interfaces related to an epp link for each e-node.
        # This is done to avoid problem if you want to use this function after creating other links.
            
            # Find itf1 interfaces related to the link and delate it.
            for eppLink in en.listEPPL:
                eppLink.en.listItf1.remove(eppLink.itf1Source)
                eppLink.itf1Dest.en.listItf1.remove(eppLink.itf1Dest)

            # clear en remove existind epp links to avoid problems.
            en.listEPPL.clear()
            en.nbEPPLink = 0

        # Initiate the endpoint_pair_links for each e-node
        for enSource in self.listEN:
            destinationList = []
            destinationListPrintable = []
            
            # Create a destination list. 
            # Here -> all the others E-nodes except itself  
            for enDest in self.listEN:
                if not enDest.id == enSource.id:
                    destinationList.append(enDest)
                    destinationListPrintable.append(enDest.name)
            
            self.logger.data(f"EN destination list for {enDest.name} is {destinationListPrintable}.")

            # Start init links with the defined destination list
            # Create 2 interfaces ITF1 (enode to enode). One is the source and one is for the destination. 
            for enDest in destinationList:
                
                itf1Dest = enDest.createITF1Interface()
                itf1Source = enSource.createITF1Interface()

                enSource.createEPPLink(itf1Source,itf1Dest)