from library.PCElements.link import Link

class Pcn1_link(Link):
    """ Class definition of a Pcn1_link with a Link heritage """

    def __init__(self, pcs, en, itf1Source, itf1Dest, number):
        """ Constructor """

        # Heritage
        super().__init__(pcs, en)
        
        self.name = 'Pcn1L-' + str(en.name)+ "." + str(itf1Dest.en.name)+ ".l" + str(number)
        self.itf1Source = itf1Source
        self.itf1Dest = itf1Dest
        # Update the logger name of the link (pcs,source,destination,id)
        self.logger.name = self.name
        
        self.logger.info(f"Pcn1L {pcs.name} - (s){en.name} --> {itf1Dest.en.name}(d) - linkNumber:{number} has been created.")