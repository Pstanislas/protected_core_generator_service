# Log imports
import logging
import logging.config
import uuid

from library.PCElements import p_functionality as PF
class Colored_cloud:
    
    def __init__(self, number, nbPf, color):
        """ Constructor """
        
        self.logger = logging.getLogger('ColoredCloud')
        self.logger.name = 'CC-' + str(number)

        self.id = str(uuid.uuid4())
        self.name = 'cc'+str(number)
        self.color = color
        self.listPF = []

        self.logger.info(f"Colored cloud {self.name} has been created.")

    # ---------------------------------------------
    # p-functionality related functions -------------------
    #----------------------------------------------

    def addPF(self, pf):
        """ Add an p-functionality node to the p-functionality node list of the PCS"""
        self.listPF.append(pf)
        self.logger.info(f"{pf.name} has been added to the p-functionality node list.")

    def createPF(self):
        """ Create and add one p-functionality node """

        number = len(self.listPF)
        pf = PF.P_functionality(cc=self,number=number)
        self.addPF(pf)

    def createMultiplePF(self, nbPf):
        """ Create nbPf p-functionality nodes and add it to the p-functionality node PCS list """

        for number in range(0,nbPf):
            self.createPF()

        self.logger.info(f"{nbPf} p-functionality nodes has been added.")

    # ---------------------------------------------
    # Links related functions ----------------------
    #----------------------------------------------

    def createFullMeshEPPLinks(self):
        """ Create or recreate a full mesh topology endpoint_pair_links on the CC """

        for pf in self.listPF:
        # Remove itf1 interfaces related to an epp link for each pf-node.
        # This is done to avoid problem if you want to use this function after creating other links.
            
            # Find itf1 interfaces related to the link and delate it.
            for eppLink in pf.listEPPL:
                eppLink.pf.listItf1.remove(eppLink.itf1Source)
                eppLink.itf1Dest.pf.listItf1.remove(eppLink.itf1Dest)

            # clear pf remove existind epp links to avoid problems.
            pf.listEPPL.clear()
            pf.nbEPPLink = 0

        # Initiate the pf endpoint_pair_links for each pf-node
        for pfSource in self.listPF:
            destinationList = []
            destinationListPrintable = []
            
            # Create a destination list. 
            # Here -> all the others E-nodes except itself  
            for pfDest in self.listPF:
                if not pfDest.id == pfSource.id:
                    destinationList.append(pfDest)
                    destinationListPrintable.append(pfDest.name)
            
            self.logger.data(f"PF destination list for {pfDest.name} is {destinationListPrintable}.")

            # Start init links with the defined destination list
            # Create 2 interfaces ITF1 (pfnode to pfnode). One is the source and one is for the destination. 
            for pfDest in destinationList:
                
                itf1Dest = pfDest.createITF1Interface()
                itf1Source = pfSource.createITF1Interface()

                pfSource.createEPPLink(itf1Source,itf1Dest)