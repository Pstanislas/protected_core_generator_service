# Log imports
import logging
import logging.config
import uuid

from library.PCElements import pcn2_link as pcn2Link
from library.PCElements import itf_interop_2 as Ift2
from library.PCElements import itf_interop_1 as Ift1
from library.PCElements import endpoint_pair_link as EPPLink

class P_functionality:
    """ Class definition of a P_functionality node """

    def __init__(self, cc, number):
        """ Constructor """

        self.id = str(uuid.uuid4())
        self.name = 'PF-' + str(cc.name) + "." + str(number)
        self.cc = cc

        # Init the P_functionality node instance object related logger.
        self.logger = logging.getLogger('pFunctionality')
        self.logger.name = self.name

        self.listItf2 = []
        self.nbItf2 = 0
        
        self.listPCN2L = []
        self.nbPCN2Link = 0

        self.listEPPL = []
        self.nbEPPLink = 0

        self.listItf1 = []
        self.nbItf1 = 0

        self.logger.info(f"{self.name} has been created.")

    def createEPPLink(self,itf1Source, itf1Dest):
        """ Create and add an endpoint_pair_links between two interfaces """
        link = EPPLink.Endpoint_pair_link(self.cc, self, itf1Source, itf1Dest, self.nbEPPLink)
        self.listEPPL.append(link)
        self.nbEPPLink+=1

    def createPcn2Link(self, itf2Source, itf1Dest):
        """ Create and add an pcn1_link between two interfaces """
        link = pcn2Link.Pcn2_link(self.cc, self, itf2Source, itf1Dest, self.nbPCN2Link)
        self.listPCN2L.append(link)
        self.nbPCN2Link+=1

    def createITF2Interface(self):
        """ Create and add an ITF1 interface and return it """
        newItf2 = Ift2.Itf_interop_2(self.cc,self,self.nbItf2)
        self.listItf2.append(newItf2)
        self.nbItf2+=1
        
        return newItf2

    def createITF1Interface(self):
        """ Create and add an ITF1 interface and return it """
        newItf1 = Ift1.Itf_interop_1(self.cc,self,self.nbItf1)
        self.listItf1.append(newItf1)
        self.nbItf1+=1
        
        return newItf1
        