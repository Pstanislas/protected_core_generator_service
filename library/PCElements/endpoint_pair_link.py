# Log imports
import logging
import logging.config
import uuid

from library.PCElements.link import Link

class Endpoint_pair_link(Link):
    """ Class definition of a Endpoint_pair_link with a Link heritage """

    def __init__(self, pcs, en, itf1Source, itf1Dest, number):
        """ Constructor """

        # Heritage
        super().__init__(pcs, en)
        
        self.name = 'EPPL-' + str(en.name)+ "." + str(itf1Dest.en.name)+ ".l" + str(number)
        self.itf1Source = itf1Source
        self.itf1Dest = itf1Dest
        # Update the logger name of the link (pcs,source,destination,id)
        self.logger.name = self.name
        
        self.logger.info(f"EPPLink {pcs.name} - (s){en.name} --> {itf1Dest.en.name}(d) - linkNumber:{number} has been created.")