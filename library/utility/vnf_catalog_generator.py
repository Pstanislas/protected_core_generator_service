import logging.config
import json

from library.PCElements import vnf as VNF 

from configparser import ConfigParser

vnfGeneratorLogger = logging.getLogger('config')

def generate_vnf_catalog(paramDictionary):

    vnfGeneratorLogger.info(f"Start to generate the vnf Catalog")
    vnfCatalog = []

    # Open and load the vnfCatalog Json file
    vnfCatalogPath = paramDictionary['OTHER'].get('vnfCatalogPath')
    with open(vnfCatalogPath) as jsonArchitecture:
        vnfJsonCatalog = json.load(jsonArchitecture)
        vnfGeneratorLogger.info(f'Json vnf catalog at {vnfCatalogPath} now open.')

    vnfGeneratorLogger.info(f"Start to generate {vnfJsonCatalog['nbVnf']} vnf")

    for vnf in vnfJsonCatalog['vnfList']:
        vnf = VNF.Vnf(vnf['vnfName'],vnf['vnfCost'])
        vnfCatalog.append(vnf)

    return vnfCatalog