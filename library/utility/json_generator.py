# Copyright 2022 Pedebearn Stanislas.
# Licensed under the Apache License, Version 2.0 (the "License");

# Log imports
import logging
import logging.config

import json
import time
import os


generatorLogger = logging.getLogger('generator')

def jsonGenerator(ProtectedCore, fileName, path):
    """ Function to generate a json file regarding the PC architecture generated """

    generatorLogger.info(f"Starting json generation.")

    # create a protected core dictionary
    pcDict = dict()
    pcDict["pcId"] = ProtectedCore.id
    pcDict["pcName"] = ProtectedCore.name
    pcDict["nbPCS"] = len(ProtectedCore.PCSList)
    pcDict["nbCC"] = len(ProtectedCore.CCList)
    
    pcDict["pcsList"] = []
    pcDict["ccList"] = []
    
    # for each PCS
    for pcs in ProtectedCore.PCSList:
        
        pcsInfo = dict()

        pcsInfo['pcsName'] = pcs.name
        pcsInfo['id'] = pcs.id
        pcsInfo['nbEn'] = len(pcs.listEN)
        pcsInfo['ENList'] = []

        # Get E-nodes information
        for en in pcs.listEN:

            enInfo = dict()
            enInfo['enName'] = en.name
            enInfo['id'] = en.id
            enInfo['nbEppLink'] = en.nbEPPLink  
            enInfo['nbItf1'] = en.nbItf1
            enInfo['isIntelligent'] = en.isIntelligent
            enInfo['capacity'] = en.capacity
            enInfo['vnfAvailabeList'] = []
            enInfo['eppLinkList'] = []
            enInfo['pcn1LinkList'] = []
            enInfo['pcn2LinkList'] = []

            for eppLink in en.listEPPL:

                linkInfo = dict()
                linkInfo['linkName'] = eppLink.name
                linkInfo['id'] = eppLink.id
                linkInfo['capacity'] = eppLink.capacity
                linkInfo['idEnSource'] = eppLink.en.id
                linkInfo['idEnDest'] = eppLink.itf1Dest.en.id
                

                enInfo['eppLinkList'].append(linkInfo)
            
            for pcn1Link in en.listPCN1L:

                linkInfo = dict()
                linkInfo['linkName'] = pcn1Link.name
                linkInfo['id'] = pcn1Link.id
                linkInfo['capacity'] = pcn1Link.capacity
                linkInfo['idEnSource'] = pcn1Link.en.id
                linkInfo['idEnDest'] = pcn1Link.itf1Dest.en.id

                enInfo['pcn1LinkList'].append(linkInfo)
            
            for pcn2Link in en.listPCN2L:

                linkInfo = dict()
                linkInfo['linkName'] = pcn2Link.name
                linkInfo['id'] = pcn2Link.id
                linkInfo['capacity'] = pcn2Link.capacity
                linkInfo['idEnSource'] = pcn2Link.en.id
                linkInfo['idPfDest'] = pcn2Link.itfDest.pf.id

                enInfo['pcn2LinkList'].append(linkInfo)

            for availableVnf in en.vnfAvailabeCatalog:

                vnfInfo = dict()
                vnfInfo['vnfName'] = availableVnf.name
                vnfInfo['id'] = availableVnf.id
                vnfInfo['cost'] = availableVnf.cost

                enInfo['vnfAvailabeList'].append(vnfInfo)
            
            pcsInfo['ENList'].append(enInfo)
            
        pcDict["pcsList"].append(pcsInfo)

    # for each CC
    for cc in ProtectedCore.CCList:
        
        ccInfo = dict()

        ccInfo['ccName'] = cc.name
        ccInfo['id'] = cc.id
        ccInfo['color'] = cc.color
        ccInfo['nbPF'] = len(cc.listPF)
        ccInfo['PFList'] = []
        

        # Get p-functionality information
        for pf in cc.listPF:
            
            pfInfo = dict()
            
            pfInfo['pfName'] = pf.name
            pfInfo['id'] = pf.id
            pfInfo['nbEppLink'] = pf.nbPCN2Link  
            pfInfo['nbItf2'] = pf.nbItf2
            pfInfo['pcn2LinkList'] = []
            pfInfo['eppLinkList'] = []

            for pcn2Link in pf.listPCN2L:

                linkInfo = dict()
                linkInfo['linkName'] = pcn2Link.name
                linkInfo['id'] = pcn2Link.id
                linkInfo['idPfSource'] = pcn2Link.en.id # en but in reality not a en ...
                linkInfo['idEnDest'] = pcn2Link.itfDest.en.id

                pfInfo['pcn2LinkList'].append(linkInfo)
            
            for eppLink in pf.listEPPL:

                linkInfo = dict()
                linkInfo['linkName'] = eppLink.name
                linkInfo['id'] = eppLink.id
                linkInfo['capacity'] = eppLink.capacity
                linkInfo['idPfSource'] = eppLink.en.id #en but in reality it's a pf ...
                linkInfo['idPfDest'] = eppLink.itf1Dest.en.id

                pfInfo['eppLinkList'].append(linkInfo)

            ccInfo['PFList'].append(pfInfo)

        pcDict["ccList"].append(ccInfo)

    # translate the dictionary into a Json file and write it
    jsonFileName = f"{path}{fileName}.json"

    with open(jsonFileName,'w', encoding='utf-8') as file:
        json.dump(pcDict,file,ensure_ascii=False, indent=4)

    generatorLogger.info(f"Generation done.")