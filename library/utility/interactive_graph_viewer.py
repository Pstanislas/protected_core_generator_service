# Copyright 2022 Pedebearn Stanislas
# Licensed under the Apache License, Version 2.0 (the "License");

import networkx as nx
from pyvis.network import Network

import logging.config
import json
import random

viewerLogger = logging.getLogger('interactiveViewer')

def displayGenerator(fileName, path, capacityRelativeDisplay):

    viewerLogger.info(f'Start display process on : {path}{fileName}.json .')
    jsonAddress = f'{path}{fileName}.json'
    
    # Open and load the generated Json file
    with open(jsonAddress) as jsonArchitecture:
        data = json.load(jsonArchitecture)
        viewerLogger.info(f'Json open.')

    # Create a networkX graph
    pcGraph = nx.MultiDiGraph()

    # Start reading the Json file to generate a networkX graph
    # For each pcs
    viewerLogger.info(f'Start reading the Json file to create a networkX Graph.')
    for pcs in data['pcsList']:
        
        viewerLogger.info(f"Making {pcs['pcsName']}")
        
        # Generate a random color.
        hexadecimalColor = ["#"+''.join([random.choice('ABCDEF0123456789') for i in range(6)])]
        viewerLogger.data(f"pcs color = {hexadecimalColor}")

        # add enodes
        for i, en in enumerate(pcs['ENList']):
            
            vnfListText = "-vnfLIst: <br>"

            for vnf in en['vnfAvailabeList']:
                vnfText = f"  -> {vnf['vnfName']}, c={vnf['cost']}<br>"
                vnfListText += vnfText
    
            title = f"""<b>{en['enName']}</b><br>
                -GlobalCapacity = {en['capacity']}<br>
                -IsIntelligent = {en['isIntelligent']}<br>
                {vnfListText}"""

            if capacityRelativeDisplay :
                # If relativeDisplay -> add node with relative size
                pcGraph.add_node(en['id'],size=25, color=hexadecimalColor, id=en['id'], label=en['enName'], title=title, group=pcs['pcsName'],value=en['capacity'])
            
            else :
                #Fixed size
                if en['isIntelligent']:
                    pcGraph.add_node(en['id'],size=30, color=hexadecimalColor, id=en['id'], label=en['enName'], title=title, group=pcs['pcsName'])
                else:
                    pcGraph.add_node(en['id'], color=hexadecimalColor, id=en['id'], label=en['enName'], title=en['enName'], group=pcs['pcsName'])

        # add links
        for en in pcs['ENList']:

            for link in en['eppLinkList']:

                title = f"""<b>{link['linkName']}</b><br>
                Capacities:<br>
                -GlobalCapacity = {link['capacity']}<br>
                -Bandwidth = 0<br>
                -Ping = 0"""

                if capacityRelativeDisplay :
                # If relativeDisplay -> add node with relative size
                    pcGraph.add_edge(link['idEnSource'],link['idEnDest'],label=link['linkName'],title=title,weight='',value=link['capacity'])

                else :
                    #Fixed size
                    pcGraph.add_edge(link['idEnSource'],link['idEnDest'],label=link['linkName'],title=title,weight='',width=3)
                
                viewerLogger.data(f"---- link {link['linkName']} --------------")
                viewerLogger.data(f"id :{link['id']}")
                viewerLogger.data(f"(s){link['idEnSource']} ---> (d){link['idEnDest']}")

            for link in en['pcn1LinkList']:

                title = f"""<b>{link['linkName']}</b><br>
                Capacities:<br>
                -GlobalCapacity = {link['capacity']}<br>
                -Bandwidth = 0<br>
                -Ping = 0"""

                if capacityRelativeDisplay :
                    # If relativeDisplay -> add node with relative size
                    pcGraph.add_edge(link['idEnSource'],link['idEnDest'],label=link['linkName'],title=title,weight='',value=link['capacity'])
                else :
                    #Fixed size
                    pcGraph.add_edge(link['idEnSource'],link['idEnDest'],label=link['linkName'],title=title,weight='')

                viewerLogger.data(f"---- link {link['linkName']} --------------")
                viewerLogger.data(f"id :{link['id']}")
                viewerLogger.data(f"(s){link['idEnSource']} ---> (d){link['idEnDest']}")

            for link in en['pcn2LinkList']:

                title = f"""<b>{link['linkName']}</b><br>
                Capacities:<br>
                -GlobalCapacity = {link['capacity']}<br>
                -Bandwidth = 0<br>
                -Ping = 0"""

                if capacityRelativeDisplay :
                    # If relativeDisplay -> add node with relative size
                    pcGraph.add_edge(link['idEnSource'],link['idPfDest'],dashes=True,label=link['linkName'],title=title,weight='',value=link['capacity'])                
                else :
                    #Fixed size
                    pcGraph.add_edge(link['idEnSource'],link['idPfDest'],dashes=True,label=link['linkName'],title=title,weight='')

                viewerLogger.data(f"---- link {link['linkName']} --------------")
                viewerLogger.data(f"id :{link['id']}")
                viewerLogger.data(f"(s){link['idEnSource']} ---> (d){link['idPfDest']}")

    
    # For each cc
    ccColors = []
    # Fix the first color to Red
    ccColors.append("#FF0000")
    # Fix the Second color to Blue
    ccColors.append("#0064ff")
    # Fix the Third color to Green
    ccColors.append("#1bff00")

    # generate 100 colors more for coloredClouds
    for color in range(0,100):
        hexadecimalColor = ["#"+''.join([random.choice('ABCDEF0123456789') for i in range(6)])]
        ccColors.append(hexadecimalColor)


    for cc in data['ccList']:
        
        viewerLogger.info(f"Making {cc['ccName']}")
        
        # Generate a random color.
        color = ccColors[cc['color']]
        viewerLogger.data(f"cc color = {color}")

        # add p-functionality nodes
        for i, pf in enumerate(cc['PFList']):
            pcGraph.add_node(pf['id'],image="../test.png",color=color, id=pf['id'], label=pf['pfName'], title=pf['pfName'], group=cc['ccName'], size=50)

            for link in pf['pcn2LinkList']:

                title = f"""<b>{link['linkName']}</b><br>
                Capacities:<br>
                -Bandwidth = 0<br>
                -Ping = 0"""

                pcGraph.add_edge(link['idPfSource'],link['idEnDest'],dashes=True,label=link['linkName'],title=title,weight='')
                
                viewerLogger.data(f"---- link {link['linkName']} --------------")
                viewerLogger.data(f"id :{link['id']}")
                viewerLogger.data(f"(s){link['idPfSource']} ---> (d){link['idEnDest']}")
            
            for link in pf['eppLinkList']:

                title = f"""<b>{link['linkName']}</b><br>
                Capacities:<br>
                -GlobalCapacity = {link['capacity']}<br>
                -Bandwidth = 0<br>
                -Ping = 0"""

                if capacityRelativeDisplay :
                # If relativeDisplay -> add node with relative size
                    pcGraph.add_edge(link['idPfSource'],link['idPfDest'],label=link['linkName'],title=title,weight='',value=link['capacity'])

                else :
                    #Fixed size
                    pcGraph.add_edge(link['idPfSource'],link['idPfDest'],label=link['linkName'],title=title,weight='',width=3)
                
                viewerLogger.data(f"---- link {link['linkName']} --------------")
                viewerLogger.data(f"id :{link['id']}")
                viewerLogger.data(f"(s){link['idPfSource']} ---> (d){link['idPfDest']}")
        
    # Create user interface
    nt = Network('95%','100%', directed=True)

    nt.set_options("""
    var options = {
        "nodes": {
            "font": {
            
            "strokeWidth": 6
            }
        },
        "edges": {
            "arrows": {
            "to": {
                "enabled": true,
                "scaleFactor": 0.45
            }
            },
            "color": {
            "inherit": true
            },
            "smooth": {
            "type": "curvedCW",
            "forceDirection": "none",
            "roundness": 0.1
            }
        },
        "interaction": {
            "hover": true,
            "navigationButtons": true
        },
        "physics": {
            "repulsion": {
            "centralGravity": 0,
            "springLength": 310,
            "springConstant": 0.015,
            "nodeDistance": 1800
            },
            "maxVelocity": 100,
            "minVelocity": 0,
            "solver": "repulsion",
            "timestep": 1
        }
    }
    """)
    

    # Add the graph from NetworkX
    #nt = Network('300%','70%', directed=True)
    nt.from_nx(pcGraph)
    
    # Curved edges
    #nt.set_edge_smooth('dynamic')

    # Display Options
    #nt.show_buttons()

    # Display
    viewerLogger.info(f'Open the visual user interface.')
    nt.show(f'{path}{fileName}.html')
