# Copyright 2022 Pedebearn Stanislas.
# Licensed under the Apache License, Version 2.0 (the "License");

# Log imports
import logging.config

from configparser import ConfigParser

paramLogger = logging.getLogger('config')

parser = ConfigParser()
parser.read('params.conf')

def setup_param():
    """ Function to setup all options """
    
    paramLogger.info(f"Start reading param file.")
    globalDictionary = dict()

    ccDictionary = setup_cc_param()
    pcsDictionary = setup_pcs_param()
    capacityDictionary = setup_capacity_param()
    otherDictionary = setup_other_param()
    
    globalDictionary['CCINFO']=ccDictionary
    globalDictionary['PCSINFO']=pcsDictionary
    globalDictionary['CAPACITY']=capacityDictionary
    globalDictionary['OTHER']=otherDictionary

    paramLogger.info(f"Params = {globalDictionary}")
    paramLogger.info(f"Param file done.")

    return globalDictionary

def setup_pcs_param():
    pcsDict = dict()

    # set nbpCS
    nbPCS = __getParam(paramValueName='nbPCS',categoryName='PCSINFO',defaultValue=4,condition=intSupEgal0)
    pcsDict['nbPCS'] = nbPCS

    # set nbENMin
    nbENMin = __getParam(paramValueName='nbENMin',categoryName='PCSINFO',defaultValue=4,condition=intSupEgal1)
    pcsDict['nbENMin'] = nbENMin

    # set nbENMax
    nbENMax = __getParam(paramValueName='nbENMax',categoryName='PCSINFO',defaultValue=7,condition=intSupEgal1)
    pcsDict['nbENMax'] = nbENMax

    # set nbMinRelation
    nbMinRelation = __getParam(paramValueName='nbMinRelation',categoryName='PCSINFO',defaultValue=1,condition=intSupEgal0)
    pcsDict['nbMinRelation'] = nbMinRelation

    # set nbMaxRelation
    nbMaxRelation = __getParam(paramValueName='nbMaxRelation',categoryName='PCSINFO',defaultValue=1,condition=intSupEgal0)
    pcsDict['nbMaxRelation'] = nbMaxRelation

    return pcsDict

def setup_capacity_param():
    capacityDict = dict()

    # set nbMinVnfNode
    nbMinVnfNode = __getParam(paramValueName='nbMinVnfNode',categoryName='CAPACITY',defaultValue=0,condition=intSupEgal0)
    capacityDict['nbMinVnfNode'] = nbMinVnfNode

    # set nbMaxVnfNode
    nbMaxVnfNode = __getParam(paramValueName='nbMaxVnfNode',categoryName='CAPACITY',defaultValue=0,condition=intSupEgal0)
    capacityDict['nbMaxVnfNode'] = nbMaxVnfNode

    # set minVnfCapacity
    minVnfCapacity = __getParam(paramValueName='minVnfCapacity',categoryName='CAPACITY',defaultValue=0,condition=intSupEgal0)
    capacityDict['minVnfCapacity'] = minVnfCapacity

    # set maxVnfCapacity
    maxVnfCapacity = __getParam(paramValueName='maxVnfCapacity',categoryName='CAPACITY',defaultValue=0,condition=intSupEgal0)
    capacityDict['maxVnfCapacity'] = maxVnfCapacity

    # set minVnfAvailableFromCatalog
    minVnfAvailableFromCatalog = __getParam(paramValueName='minVnfAvailableFromCatalog',categoryName='CAPACITY',defaultValue=1,condition=intSupEgal1)
    capacityDict['minVnfAvailableFromCatalog'] = minVnfAvailableFromCatalog

    # set maxVnfAvailableFromCatalog
    maxVnfAvailableFromCatalog = __getParam(paramValueName='maxVnfAvailableFromCatalog',categoryName='CAPACITY',defaultValue=3,condition=intSupEgal1)
    capacityDict['maxVnfAvailableFromCatalog'] = maxVnfAvailableFromCatalog

    # set minPcs1LinkCapacity
    minPcs1LinkCapacity = __getParam(paramValueName='minPcs1LinkCapacity',categoryName='CAPACITY',defaultValue=0,condition=intSupEgal0)
    capacityDict['minPcs1LinkCapacity'] = minPcs1LinkCapacity

    # set maxPcs1LinkCapacity
    maxPcs1LinkCapacity = __getParam(paramValueName='maxPcs1LinkCapacity',categoryName='CAPACITY',defaultValue=0,condition=intSupEgal0)
    capacityDict['maxPcs1LinkCapacity'] = maxPcs1LinkCapacity

    # set minPcs2LinkCapacity
    minPcs2LinkCapacity = __getParam(paramValueName='minPcs2LinkCapacity',categoryName='CAPACITY',defaultValue=0,condition=intSupEgal0)
    capacityDict['minPcs2LinkCapacity'] = minPcs2LinkCapacity

    # set maxPcs2LinkCapacity
    maxPcs2LinkCapacity = __getParam(paramValueName='maxPcs2LinkCapacity',categoryName='CAPACITY',defaultValue=0,condition=intSupEgal0)
    capacityDict['maxPcs2LinkCapacity'] = maxPcs2LinkCapacity

    # set minEppLinkCapacity
    minEppLinkCapacity = __getParam(paramValueName='minEppLinkCapacity',categoryName='CAPACITY',defaultValue=0,condition=intSupEgal0)
    capacityDict['minEppLinkCapacity'] = minEppLinkCapacity

    # set maxEppLinkCapacity
    maxEppLinkCapacity = __getParam(paramValueName='maxEppLinkCapacity',categoryName='CAPACITY',defaultValue=0,condition=intSupEgal0)
    capacityDict['maxEppLinkCapacity'] = maxEppLinkCapacity

    return capacityDict

def setup_cc_param():
    ccDict = dict()

    # set nbCC
    nbCC = __getParam(paramValueName='nbcc',categoryName='CCINFO',defaultValue=2,condition=intSupEgal0)
    ccDict['nbCC'] = nbCC

    # set nbMinRelationCC
    nbMinRelationCC = __getParam(paramValueName='nbMinRelationCC',categoryName='CCINFO',defaultValue=1,condition=intSupEgal0)
    ccDict['nbMinRelationCC'] = nbMinRelationCC

    # set nbMaxRelationCC
    nbMaxRelationCC = __getParam(paramValueName='nbMaxRelationCC',categoryName='CCINFO',defaultValue=2,condition=intSupEgal0)
    ccDict['nbMaxRelationCC'] = nbMaxRelationCC

    # set nbPFMin
    nbPFMin = __getParam(paramValueName='nbPFMin',categoryName='CCINFO',defaultValue=1,condition=intSupEgal1)
    ccDict['nbPFMin'] = nbPFMin

    # set nbPFMax
    nbPFMax = __getParam(paramValueName='nbPFMax',categoryName='CCINFO',defaultValue=1,condition=intSupEgal1)
    ccDict['nbPFMax'] = nbPFMax

    # set nbColor
    nbColor = __getParam(paramValueName='nbColor',categoryName='CCINFO',defaultValue=1,condition=intSupEgal1)
    ccDict['nbColor'] = nbColor

    return ccDict

def setup_other_param():
    otherDict = dict()

    # set fileName
    fileName = __getStringParam(paramValueName='fileName',categoryName='OTHER',defaultValue='softanetArchitecture')
    otherDict['fileName'] = fileName

    # set vnfCatalogPath
    vnfCatalogPath = __getStringParam(paramValueName='vnfCatalogPath',categoryName='OTHER',defaultValue='./vnf_catalog.json')
    otherDict['vnfCatalogPath'] = vnfCatalogPath

    # set newFolder
    newFolder = __getBoolParam(paramValueName='newFolder',categoryName='OTHER',defaultValue=False)
    otherDict['newFolder'] = newFolder

    # set display
    display = __getBoolParam(paramValueName='display',categoryName='OTHER',defaultValue=False)
    otherDict['display'] = display

    # set capacityRelativeDisplay
    capacityRelativeDisplay = __getBoolParam(paramValueName='capacityRelativeDisplay',categoryName='OTHER',defaultValue=False)
    otherDict['capacityRelativeDisplay'] = capacityRelativeDisplay

    return otherDict

def intSupEgal0(var):
    if(isinstance(var, int) and var >= 0):
        return True
    else:
        return False

def intSupEgal1(var):
    if(isinstance(var, int) and var > 0):
        return True
    else:
        return False

def __getParam(paramValueName, categoryName, defaultValue, condition):
    """ Generic Setup paramValue variable """
    
    paramValue = defaultValue

    try :
        
        # get value from conf file
        paramValue = parser.getint(categoryName, paramValueName) 

        # check if condition is of
        if condition:
            paramLogger.info(f"{paramValueName} set to {paramValue}")

        else:
            raise ValueError

    # If invalid number
    except:
        
        # Update the conf file
        try :
            parser.add_section(categoryName) # -> error if already exist
        except:
            pass

        parser.set(categoryName,paramValueName,str(defaultValue))
        
        # write in the conf file
        with open('params.conf','w') as configfile:
            parser.write(configfile)

        paramLogger.error(f"{paramValueName} invalid value -> {paramValue}. It has been update to {defaultValue}.")
    
    return paramValue

def __getStringParam(paramValueName, categoryName, defaultValue):
    """ Generic Setup paramValue for a String variable """
    
    paramValue = defaultValue

    try :
        
        # get value from conf file
        paramValue = parser.get(categoryName, paramValueName) 
        paramLogger.info(f"{paramValueName} set to {paramValue}")

    # If invalid number
    except:
        
        # Update the conf file
        try :
            parser.add_section(categoryName) # -> error if already exist
        except:
            pass

        parser.set(categoryName,paramValueName,defaultValue)
        
        # write in the conf file
        with open('params.conf','w') as configfile:
            parser.write(configfile)

        paramLogger.error(f"{paramValueName} invalid value -> {paramValue}. It has been update to {defaultValue}.")
    
    return paramValue

def __getBoolParam(paramValueName, categoryName, defaultValue):
    """ Generic Setup paramValue for a String variable """
    
    paramValue = defaultValue

    try :
        
        # get value from conf file
        paramValue = parser.getboolean(categoryName, paramValueName) 
        paramLogger.info(f"{paramValueName} set to {paramValue}")

    # If invalid number
    except:
        
        # Update the conf file
        try :
            parser.add_section(categoryName) # -> error if already exist
        except:
            pass

        parser.set(categoryName,paramValueName,str(defaultValue))
        
        # write in the conf file
        with open('params.conf','w') as configfile:
            parser.write(configfile)

        paramLogger.error(f"{paramValueName} invalid value -> {paramValue}. It has been update to {defaultValue}.")
    
    return paramValue

setup_param()