# Copyright 2022 Pedebearn Stanislas.
# Licensed under the Apache License, Version 2.0 (the "License");

# Log imports
import logging
import logging.config
import os
import time

from library.PCElements import protected_core as PC

from library.utility.json_generator import jsonGenerator
from library.utility.interactive_graph_viewer import displayGenerator
from library.utility import setup_param as setup
from library.utility import vnf_catalog_generator as vnfGenerator


paramDictionary = None
PCSList = []
CCList = []
vnfCatalog = []
ProtectedCore = None

jsonFileName = None

def startService():
    """ Function which starts the creation of the topology"""
    
    # Protected core instantiation
    global ProtectedCore
    global PCSList
    global CCList

    ProtectedCore = PC.Protected_core(PCSList=PCSList, CCList=CCList)

    # PCS creation --------------------------------
    ProtectedCore.createMultipleRandomProtectedCoreSegments(paramDictionary)
    ProtectedCore.createRandomPCN1LinkTopology(paramDictionary)
    ProtectedCore.setAllRandomPcsCapacity(paramDictionary,vnfCatalog)
    ProtectedCore.dataLogPrint()
    
    # cc creation --------------------------------
    ProtectedCore.createMultipleRandomColoredCloud(paramDictionary)
    ProtectedCore.createRandomPCN2LinkTopology(paramDictionary)
    ProtectedCore.dataLogPrint()
    

if __name__ == '__main__':
    """ Main function that setup the service """

    # create console handler with a higher log level
    DATA_LEVELV_NUM = 15
    logging.addLevelName(DATA_LEVELV_NUM, "DATA")

    def data(self, message, *args, **kws):
        
        if self.isEnabledFor(DATA_LEVELV_NUM):
            # Yes, logger takes its '*args' as 'args'.
            self._log(DATA_LEVELV_NUM, message, args, **kws)

    # Update the config path and create the generatorService logger
    logging.config.fileConfig('logging.conf')
    logging.Logger.data = data

    generatorServiceLogger = logging.getLogger('generatorService')
    
    generatorServiceLogger.error("Logger error Ok")
    generatorServiceLogger.info("Logger info Ok")
    generatorServiceLogger.data("Logger data Ok")
    generatorServiceLogger.debug("Logger debug Ok")

    # Setup all parameters (from setup.conf file)
    paramDictionary = setup.setup_param()

    generatorServiceLogger.info("Service init Ok")

    # Generate the vnfCatalog
    vnfCatalog = vnfGenerator.generate_vnf_catalog(paramDictionary)
    
    # Create a folder if NewFolder option selected
    root = "./generatedArchitecture/"
    path = root
    fileName = paramDictionary['OTHER'].get("fileName")

    if(paramDictionary['OTHER'].get("newFolder")):
        folderName = time.strftime("%Y%m%d-%H-%M-%S")
        
        # update path with the new folder
        path = f"{root}{folderName}/"
        # create the folder
        os.mkdir(path)

    # Start topology generation
    startService()

    # Start json generation
    jsonGenerator(ProtectedCore,fileName,path)

    # Start display graphGenerator
    if(paramDictionary['OTHER'].get("display")):
        capacityRelativeDisplay = paramDictionary['OTHER'].get("capacityRelativeDisplay")
        displayGenerator(fileName, path, capacityRelativeDisplay)